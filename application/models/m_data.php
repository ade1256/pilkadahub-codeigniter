<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_data extends CI_Model{


  // LIHAT DATA QUICK COUNT
  function get_suara(){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkadahub.firebaseio.com/vote.json",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: 89355c97-f1c3-4ed1-bb91-ff5b7f19a218"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $normal =  strip_tags($response);
    $array = json_decode($normal, TRUE);

    return $array;
  }

  // GET Berita
  function tampil_data_berita(){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/news",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $normal =  strip_tags($response);
    $array = json_decode($normal, TRUE);

    return $array;
  } 

// BERITA BY ID
  function tampil_data_berita_by_id($id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/news/".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $normal =  strip_tags($response);
    $array = json_decode($normal, TRUE);

    return $array;
  } 

  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////
  // ////                        AGENDA                                         ////
  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////

  // GET Agenda
  function tampil_data_agenda_persiapan(){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/agenda/tahapan/Persiapan",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $normal =  strip_tags($response);
    $array = json_decode($normal, TRUE);

    return $array;
  } 
  function tampil_data_agenda_penyelenggaraan(){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/agenda/tahapan/Penyelenggaraan",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $normal =  strip_tags($response);
    $array = json_decode($normal, TRUE);

    return $array;
  } 

  // LIHAT AGENDA
  function lihat_agenda($id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/agenda/".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $normal =  strip_tags($response);
    $array = json_decode($normal, TRUE);

    return $array;
  } 
  // HAPUS AGENDA
  function hapus_agenda($id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/agenda/".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "DELETE",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      redirect(base_url('agenda'),'refresh');
    }
  } 

  // update agenda
  function update_agenda($id,$tgl_awal,$tgl_akhir,$detail,$tahapan){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/agenda/".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "PUT",
      CURLOPT_POSTFIELDS => '{
        "tgl_awal": "'.$tgl_awal.'",
        "tgl_akhir": "'.$tgl_akhir.'",
        "detail": "'.$detail.'",
        "tahapan": "'.$tahapan.'"

      }',
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      // echo $response;
      redirect(base_url('agenda'),'refresh');
    }
  }

  // POST AGENDA
  function kirim_agenda($tgl_awal,$tgl_akhir,$detail,$tahapan,$__v){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/agenda/",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{
        "tgl_awal": "'.$tgl_awal.'",
        "tgl_akhir": "'.$tgl_akhir.'",
        "detail": "'.$detail.'",
        "tahapan": "'.$tahapan.'",
        "__v": "'.$__v.'"

      }',
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      // echo $response;
      redirect(base_url('agenda'),'refresh');
    }
  }

   // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////
  // ////                        ADUAN                                       ////
  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////

   // GET Aduan
  function tampil_data_aduan(){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/complaint",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $normal =  strip_tags($response);
    $array = json_decode($normal, TRUE);

    return $array;
  } 
  // LIHAT ADUAN
  function lihat_aduan($id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/complaint/".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $normal =  strip_tags($response);
    $array = json_decode($normal, TRUE);

    return $array;
  }
  function update_aduan($id,$nama,$alamat,$email,$no_hp,$idPhoto,$photo,$tipe,$isi){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/complaint/".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "PUT",
      CURLOPT_POSTFIELDS => '{
        "nama": "'.$nama.'",
        "alamat": "'.$alamat.'",
        "email": "'.$email.'",
        "no_hp": "'.$no_hp.'",
        "photo": ["'.$photo.'","'.$idPhoto.'"],
        "tipe": "'.$tipe.'",
        "isi": "'.$isi.'"
        

      }',
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      // echo $response;
      redirect(base_url('aduan'),'refresh');
    }
  }
   function kirim_aduan($nama,$alamat,$email,$no_hp,$photo_key,$photo_name,$tipe,$isi){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/complaint/",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{
        "nama": "'.$nama.'",
        "alamat": "'.$alamat.'",
        "email": "'.$email.'",
        "no_hp": "'.$no_hp.'",
        "photo": [
            "'.$photo_name.'",
            "'.$photo_key.'"
        ],
        "tipe": "'.$tipe.'",
        "isi": "'.$isi.'"
      }',
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
      // redirect(base_url('aduan'),'refresh');
    }
  }

  // HAPUS ADUAN
  function hapus_aduan($id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/complaint/".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "DELETE",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      redirect(base_url('aduan'),'refresh');
    }
  } 


  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////
  // ////                       PASLON                                     ////
  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////
  // ///////////////////////////////////////////////////////////////////////////////

   // GET Paslon
  function tampil_data_paslon(){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/paslon",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $normal =  strip_tags($response);
    $array = json_decode($normal, TRUE);

    return $array;
  } 
  // LIHAT Paslon
  function lihat_paslon($id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/paslon/".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $normal =  strip_tags($response);
    $array = json_decode($normal, TRUE);

    return $array;
  }
  // HAPUS PASLON
  function hapus_paslon($id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/paslon/".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "DELETE",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: 3c9953cc-b7b7-42c8-a1f2-d28b733114a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      redirect(base_url('paslon'),'refresh');
    }
  } 

  // UPDATE PASLON
  function update_paslon($id,$nama_kepala_daerah,$gender_kepala_daerah,$tempat_lahir_kepala_daerah,$tanggal_lahir_kepala_daerah,$pekerjaan_kepala_daerah,$nama_wakil_kepala_daerah,$gender_wakil_kepala_daerah,$tempat_lahir_wakil,$tanggal_lahir_wakil,$pekerjaan_wakil_kepala_daerah,$jumlah_dukungan,$partai_pendukung,$dana_kampanye){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/paslon/".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "PUT",
      CURLOPT_POSTFIELDS => '{
        "nama_kepala_daerah": "'.$nama_kepala_daerah.'",
        "gender_kepala_daerah": "'.$gender_kepala_daerah.'",
        "tempat_lahir_kepala_daerah": "'.$tempat_lahir_kepala_daerah.'",
        "tanggal_lahir_kepala_daerah": "'.$tanggal_lahir_kepala_daerah.'",
        "pekerjaan_kepala_daerah": "'.$pekerjaan_kepala_daerah.'",
        "nama_wakil_kepala_daerah": "'.$nama_wakil_kepala_daerah.'",
        "gender_wakil_kepala_daerah": "'.$gender_wakil_kepala_daerah.'",
        "tempat_lahir_wakil": "'.$tempat_lahir_wakil.'",
        "tanggal_lahir_wakil": "'.$tanggal_lahir_wakil.'",
        "pekerjaan_wakil_kepala_daerah": "'.$pekerjaan_wakil_kepala_daerah.'",
        "jumlah_dukungan": "'.$jumlah_dukungan.'",
        "partai_pendukung": "'.$partai_pendukung.'",
        "dana_kampanye": "'.$dana_kampanye.'"
        

      }',
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      // echo $response;
      redirect(base_url('paslon'),'refresh');
    }
  }

  // KIRIM PASLON
  function kirim_paslon($nama_kepala_daerah,$gender_kepala_daerah,$tempat_lahir_kepala_daerah,$tanggal_lahir_kepala_daerah,$pekerjaan_kepala_daerah,$nama_wakil_kepala_daerah,$gender_wakil_kepala_daerah,$tempat_lahir_wakil,$tanggal_lahir_wakil,$pekerjaan_wakil_kepala_daerah,$jumlah_dukungan,$partai_pendukung,$dana_kampanye){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/paslon/",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{
        "nama_kepala_daerah": "'.$nama_kepala_daerah.'",
        "gender_kepala_daerah": "'.$gender_kepala_daerah.'",
        "tempat_lahir_kepala_daerah": "'.$tempat_lahir_kepala_daerah.'",
        "tanggal_lahir_kepala_daerah": "'.$tanggal_lahir_kepala_daerah.'",
        "pekerjaan_kepala_daerah": "'.$pekerjaan_kepala_daerah.'",
        "nama_wakil_kepala_daerah": "'.$nama_wakil_kepala_daerah.'",
        "gender_wakil_kepala_daerah": "'.$gender_wakil_kepala_daerah.'",
        "tempat_lahir_wakil": "'.$tempat_lahir_wakil.'",
        "tanggal_lahir_wakil": "'.$tanggal_lahir_wakil.'",
        "pekerjaan_wakil_kepala_daerah": "'.$pekerjaan_wakil_kepala_daerah.'",
        "jumlah_dukungan": "'.$jumlah_dukungan.'",
        "partai_pendukung": "'.$partai_pendukung.'",
        "dana_kampanye": "'.$dana_kampanye.'"
        
        
      }',
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      // echo $response;
      redirect(base_url('paslon'),'refresh');
    }
  }
}

