<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		if ($this->session->userdata('status')!='login' || $this->session->userdata('auth')!='pilkadahub') {
			redirect(base_url('login'));
		}
	}


	public function index(){
		$data['title'] = 'Overview';
		$data['suara'] = $this->M_data->get_suara();
		$this->load->view('v_home',$data);
	}

	function quickcount($id){
		echo "Maaf data quickcount daerah ";
		echo $id;
		echo " belum tersedia.";
		echo "<br><br> Halaman ini akan segera datang. Silahkan kembali ke ";
		echo "<a href='".base_url('home')."' class='btn btn-primary'>Home</a>";
	}
}
