<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
	}

	public function index(){
		$data['title'] = 'Login';
		$this->load->view('v_login',$data);
	}

	function logout(){
		$this->session->sess_destroy('status','email','level');
		redirect('login','refresh');
	}
}
