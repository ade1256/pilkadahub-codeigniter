<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		if ($this->session->userdata('status')!='login') {
			redirect(base_url('login'));
		}
	}

	public function index(){
		// $data['user'] = $this->M_data->tampil_data_user();
		$data['title'] = 'User';
		$this->load->view('v_user',$data);
	}

	function edit($id){
		if ($id == "action") {
			# aksi edit
		}else {
			$data['title'] = 'Edit User';
			$data['uid'] = $id;
			$this->load->view('v_user_edit',$data);	
		}
	}

	function tambah(){
		$data['title'] = 'Tambah User';
		$this->load->view('v_user_tambah',$data);	
	}
}
