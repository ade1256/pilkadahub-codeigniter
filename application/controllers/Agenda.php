<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		if ($this->session->userdata('status')!='login') {
			redirect(base_url('login'));
		}
	}
	public function index(){
		$data['agenda_persiapan'] = $this->M_data->tampil_data_agenda_persiapan();
		$data['agenda_penyelenggaraan'] = $this->M_data->tampil_data_agenda_penyelenggaraan();
		$data['title'] = 'Agenda';
		$this->load->view('v_agenda',$data);
	}

	function hapus($id){
		$this->M_data->hapus_agenda($id);
	}

	function edit($id){
		$data['agenda'] = $this->M_data->lihat_agenda($id);
		$data['title'] = 'Edit Agenda';
		$this->load->view('v_agenda_edit',$data);
	}
	function tambah(){
		$data['title'] = 'Tambah Agenda';
		$this->load->view('v_agenda_tambah',$data);
	}

	function kirim(){
		$tgl_awal = $this->input->post('tgl_awal');
		$detail = $this->input->post('detail');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$tahapan = $this->input->post('tahapan');
		$__v = $this->input->post('__v');
		$this->M_data->kirim_agenda($tgl_awal,$tgl_akhir,$detail,$tahapan,$__v);
	}

	function update(){
		$id = $this->input->post('_id');
		$tgl_awal = $this->input->post('tgl_awal');
		$detail = $this->input->post('detail');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$tahapan = $this->input->post('tahapan');
		

		$this->M_data->update_agenda($id,$tgl_awal,$tgl_akhir,$detail,$tahapan);
	}

	function lihat($id){
		$data['agenda'] = $this->M_data->lihat_agenda($id);
		$data['title'] = 'Detail Agenda';
		$this->load->view('v_agenda_lihat',$data);
	}
}