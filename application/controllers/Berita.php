<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		if ($this->session->userdata('status')!='login') {
			redirect(base_url('login'));
		}
	}
	public function index(){
		$data['berita'] = $this->M_data->tampil_data_berita();
		$data['title'] = 'Berita';
		$this->load->view('v_berita',$data);
	}

	function lihat($id){
		$data['title'] = 'Detail Berita';
		$data['berita'] = $this->M_data->tampil_data_berita_by_id($id);
		$this->load->view('v_berita_lihat', $data);
	}

	function tambah(){
		$data['title'] = 'Tambah Berita';
		$this->load->view('v_berita_tambah', $data);
	}

	function edit($id){
		$data['title'] = 'Edit Berita';
		$data['berita'] = $this->M_data->tampil_data_berita_by_id($id);
		$this->load->view('v_berita_edit', $data);
	}

	function update(){
		$id = $this->input->post('_id');
		$tanggal = $this->input->post('tanggal');
		$title = $this->input->post('title');
		$body = $this->input->post('body');
		$tag = $this->input->post('tag');
		$img_link = $this->input->post('img_link');
		$visitor = $this->input->post('visitor');


		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/news/".$id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => '{
				"created_at": "'.$tanggal.'",
				"title": "'.$title.'",
				"body": "'.htmlentities($body).'",
				"tag": "'.$tag.'",
				"img_link": "'.$img_link.'",
				"visitor": '.$visitor.'
			}',
			CURLOPT_HTTPHEADER => array(
				"Cache-Control: no-cache",
				"Content-Type: application/json",
				"Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			// echo $response;
			redirect(base_url('berita'),'refresh');
		}
	}

	function hapus($id){
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/news/".$id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "DELETE",
			CURLOPT_HTTPHEADER => array(
				"Cache-Control: no-cache",
				"Content-Type: application/json",
				"Postman-Token: b1a1dff7-480b-4744-a31a-8c4631a742b5"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			redirect(base_url('berita'),'refresh');
		}
	}

	function kirim(){

		$tanggal = $this->input->post('tanggal');
		$title = $this->input->post('title');
		$body = $this->input->post('body');
		$tag = $this->input->post('tag');
		$img_link = $this->input->post('img_link');
		$visitor = $this->input->post('visitor');

		// echo preg_replace( "/\r|\n/", "", $body);

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/news",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => '{
				"created_at": "'.$tanggal.'",
				"title": "'.$title.'",
				"body": "'.htmlentities($body).'",
				"tag": "'.$tag.'",
				"img_link": "'.$img_link.'",
				"visitor": '.$visitor.'
			}',
			CURLOPT_HTTPHEADER => array(
				"Cache-Control: no-cache",
				"Content-Type: application/json",
				"Postman-Token: c5fa8623-96e8-49e9-9cbf-f3496f844947"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			redirect(base_url('berita'),'refresh');
		}
	}
}
