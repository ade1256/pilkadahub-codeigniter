<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $title ?>
   </h1>
   <ol class="breadcrumb">
    <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Data <?php echo $title ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data <?php echo $title ?> Persiapan</h3>
          <br/>
          <br/>
          <a href="<?php echo base_url('agenda/tambah') ?>" class="btn btn-primary">Tambah <?php echo $title ?> Persiapan</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

          <table id="datatable1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Tgl Awal</th>
                <th>Tgl Akhir</th>
                <th>Detail</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($agenda_persiapan as $persiapan) {?>
              <tr>
                <td><?php echo $persiapan['tgl_awal'] ?></td>
                <td><?php echo $persiapan['tgl_akhir'] ?></td>
                <td><?php echo $persiapan['detail'] ?></td>
                <td>
                  <a href="<?php echo base_url('agenda/lihat/'.$persiapan['_id'])?>" class="btn btn-primary green"><i class="fa fa-eye"></i></a> 
                  
                  <a href="<?php echo base_url('agenda/edit/'.$persiapan['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                  <a href="<?php echo base_url('agenda/hapus/'.$persiapan['_id'])?>" class="btn btn-primary red"><i class="fa fa-trash"></i></a> 
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data <?php echo $title ?> Penyelenggaraan</h3>
          <br/>
          <br/>
          <a href="<?php echo base_url('agenda/tambah') ?>" class="btn btn-primary">Tambah <?php echo $title ?> Penyelenggaraan</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

          <table id="datatable2" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Tgl Awal</th>
                <th>Tgl Akhir</th>
                <th>Detail</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($agenda_penyelenggaraan as $penyelenggaraan) {?>
              <tr>
                <td><?php echo $penyelenggaraan['tgl_awal'] ?></td>
                <td><?php echo $penyelenggaraan['tgl_akhir'] ?></td>
                <td><?php echo $penyelenggaraan['detail'] ?></td>
                <td>
                  <a href="<?php echo base_url('agenda/lihat/'.$penyelenggaraan['_id'])?>" class="btn btn-primary"><i class="fa fa-eye"></i></a> 
                  <a href="<?php echo base_url('agenda/hapus/'.$penyelenggaraan['_id'])?>" class="btn btn-primary"><i class="fa fa-trash"></i></a> 
                  <a href="<?php echo base_url('agenda/edit/'.$penyelenggaraan['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer'); ?>
<script>
  $(function () {
    $('#datatable1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
    $('#datatable2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>