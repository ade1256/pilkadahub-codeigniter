<?php $this->load->view('layout/header'); ?>
<script src="https://cdn.firebase.com/libs/firebaseui/2.5.1/firebaseui.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/2.5.1/firebaseui.css" />
<style type="text/css">

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $title ?>
   </h1>
   <ol class="breadcrumb">
    <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Data <?php echo $title ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data <?php echo $title ?></h3>
          <br/>
          <br/>
          <a href="<?php echo base_url('user/tambah') ?>" class="btn btn-primary">Tambah <?php echo $title ?></a>
        </div>
        <script type="text/javascript">
          var config = {
            apiKey: "AIzaSyDdBwHnppn8R_JFmff8-vuMHxSf7OLgB6I",
            authDomain: "pilkadahub.firebaseapp.com",
            databaseURL: "https://pilkadahub.firebaseio.com",
            projectId: "pilkadahub",
            storageBucket: "pilkadahub.appspot.com",
            messagingSenderId: "56408364674"
          };
          firebase.initializeApp(config);

          firebase.auth().signInWithEmailAndPassword('ade1256@mail.com', 'ade12561256').catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode === 'auth/wrong-password') {
              alert('Wrong password.');
            } else {   
              alert(errorMessage);
            }
            console.log(error);
            document.getElementById('quickstart-sign-in').disabled = false;
          });
        </script>
        <div class='box-body'>
          <table id='datatable' class='table table-bordered table-striped'>
            <thead>
              <tr>
                <th>Email</th>
                <th>Id Daerah</th>
                <th>Nama</th>
                <th>Username</th>
                <th colspan="2">Action</th>
              </tr>
            </thead>
            <tbody id="data">
              <script>
                var tblAdmin = firebase.database().ref().child("admin").orderByChild("username");
                tblAdmin.on("value", function(snap) {
                  $('#data').empty();
                  tr = "";
                  snap.forEach(function(childSnapshot){
                    element = childSnapshot.val();
                    obj = {"email":element.email,"id_daerah":element.id_daerah,"nama":element.nama,"username":element.username};
                    key = childSnapshot.key;
                    edit = "<td><a href='<?=base_url()?>user/edit/" + key + "'>Edit</td>"
                    $('#data').append('<tr><td>'+obj['email']+'</td><td>'+obj['id_daerah']+'</td><td>'+obj['nama']+'</td><td>'+obj['username']+ edit + '</tr>');
                  });
                });
              </script>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
<?php $this->load->view('layout/footer'); ?>

<script type="text/javascript">
 $(function () {
  $('#datatable').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": false,
    "ordering": false,
    "info": false,
    "autoWidth": false
  });
});
</script>