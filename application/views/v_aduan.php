<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $title ?>
   </h1>
   <ol class="breadcrumb">
    <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Data <?php echo $title ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data <?php echo $title ?></h3>
          <br/>
          <br/>
          <a href="<?php echo base_url('aduan/tambah') ?>" class="btn btn-primary">Tambah <?php echo $title ?></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="datatable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>NAMA</th>
                <th>ALAMAT</th>
                <th>HP</th>
                <th>ISI</th>
                <th>PHOTO</th>
                <th>TIPE</th>
                <th>ACTION</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $i=0;
              $total=count($aduan);
              for ($i; $i < $total ; $i++) { 
                echo '<tr>';
                echo '<td>'.$aduan[$i]['nama'].'</td>';
                echo '<td>'.$aduan[$i]['alamat'].'</td>';
                echo '<td>'.$aduan[$i]['no_hp'].'</td>';
                echo '<td>'.$aduan[$i]['isi'].'</td>';
                echo '<td><img width="60" src="https://firebasestorage.googleapis.com/v0/b/pilkadahub.appspot.com/o/laporan%2F'.$aduan[$i]['photo'][1].'%2F'.$aduan[$i]['photo'][0].'?alt=media"/></td>';
                echo '<td>'.$aduan[$i]['tipe'].'</td>';

                ?>

                <td>
                  <a href="<?php echo base_url('aduan/lihat/'.$aduan[$i]['_id'])?>" class="btn btn-primary green"><i class="fa fa-eye"></i></a> 
                 
                  <a href="<?php echo base_url('aduan/edit/'.$aduan[$i]['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                   <a href="<?php echo base_url('aduan/hapus/'.$aduan[$i]['_id'])?>" class="btn btn-primary red"><i class="fa fa-trash"></i></a> 
                </td>
                <?php
                echo '</tr>';
              }
              ?>
            </tbody>

          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer'); ?>
<script>
  $(function () {
    $('#datatable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>