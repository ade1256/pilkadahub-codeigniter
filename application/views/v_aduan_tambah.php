<?php $this->load->view('layout/header'); ?>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
<style type="text/css">
.btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  outline: none;
  background: white;
  cursor: inherit;
  display: block;
}

#img-upload{
 max-width: 300px;
}
</style>

<script type="text/javascript">
  var img;
  var upFoto = false;
  var config = {
    apiKey: "AIzaSyDdBwHnppn8R_JFmff8-vuMHxSf7OLgB6I",
    authDomain: "pilkadahub.firebaseapp.com",
    databaseURL: "https://pilkadahub.firebaseio.com",
    projectId: "pilkadahub",
    storageBucket: "pilkadahub.appspot.com",
    messagingSenderId: "56408364674"
  };
  firebase.initializeApp(config);

  function submitHandler(e){
    if ($("#photo").val() != "") {
      if (upFoto == false) {
        e.preventDefault();
        var storageRef = firebase.storage().ref();
        var keyPush = firebase.database().ref().push().key;
        storageRef.child("laporan/"+ keyPush +"/"+ $("#photo").val())
        .putString(img, 'data_url').then(function(snapshot) {
          upFoto = true;
          $("#keyImg").val(keyPush);
          $("#form").submit();
        });
      }
    }
  }
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title ?></h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo base_url('aduan')?>"> Aduan</a></li>
      <li class="active">Tambah Aduan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
       <div class="box">
        <div class="box-body">
         <form method="post" action="<?php echo base_url('aduan/kirim') ?>" onsubmit="submitHandler(event)">
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control" " />
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <input type="text" name="alamat" class="form-control" " />
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control" " />
          </div>
          <div class="form-group">
            <label>No HP</label>
            <input type="number" name="no_hp" class="form-control" " />
          </div>
          <div class="form-group">
            <label>Tipe</label>
            <select class="form-control" name="tipe">
             <option value="Kritik">Kritik</option>
             <option  value="Saran">Saran</option>
           </select>
         </div>
         <div class="form-group">
          <label>Isi</label>
          <textarea type="text" name="isi" class="form-control"></textarea>
        </div>
        <div class="form-group">
          <label>Photo</label>
          <input type="hidden" class="form-control" name="keyImg" id="keyImg">
          <div class="input-group">
            <span class="input-group-btn">
              <span class="btn btn-default btn-file">
                Browse… <input type="file" id="imgInp" name="photo">
              </span>
            </span>
            <input type="text" class="form-control" name="photo" id="photo" readonly>
          </div>  
          <?php echo '<img id="img-upload" src=""/>' ?>
        </div>

        <br>
        <br>
        <input type="submit" class="btn btn-primary" value="Simpan" />
        <a href="<?php echo base_url('aduan')?>" class="btn btn-primary">Batal</a>
      </form>
    </div>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer'); ?>
<script>
  $(document).ready( function() {
    $(document).on('change', '.btn-file :file', function() {
      var input = $(this),
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function(event, label) {

      var input = $(this).parents('.input-group').find(':text'),
      log = label;

      if( input.length ) {
        input.val(log);
      } else {
        if( log ) alert(log);
      }
      
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#img-upload').attr('src', e.target.result);
          img = e.target.result;
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imgInp").change(function(){
      readURL(this);
    });   
  });
</script>
