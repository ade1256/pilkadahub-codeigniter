<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php echo $title ?>
   </h1>
   <ol class="breadcrumb">
    <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Data <?php echo $title ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data <?php echo $title ?></h3>
          <br/>
          <br/>
          <a href="<?php echo base_url('paslon/tambah') ?>" class="btn btn-primary">Tambah <?php echo $title ?></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="datatable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>KEPALA - WAKIL</th>
                <th>GENDER</th>
                <th>TEMPAT LAHIR</th>
                <th>TANGGAL LAHIR</th>
                <th>PEKERJAAN</th>
                <th>JUMLAH DUKUNGAN</th>
                <th>PARTAI PENDUKUNG</th>
                <th>DANA KAMPANYE</th>
                <th>ACTION_BUTTON</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              foreach ($paslon as $key) {
                ?>
                <tr>
                  <td><?php echo $key['nama_kepala_daerah'].' - '.$key['nama_wakil_kepala_daerah'] ?></td>
                  <td><?php echo $key['gender_kepala_daerah'].' - '.$key['gender_wakil_kepala_daerah'] ?></td>
                  <td><?php echo $key['tempat_lahir_kepala_daerah'].' - '.$key['tempat_lahir_wakil'] ?></td>
                  <td><?php echo $key['tanggal_lahir_kepala_daerah'].' - '.$key['tanggal_lahir_wakil'] ?></td>
                  <td><?php echo $key['pekerjaan_kepala_daerah'].' - '.$key['pekerjaan_wakil_kepala_daerah'] ?></td>
                  <td><?php echo $key['jumlah_dukungan']?></td>
                  <td><?php echo $key['partai_pendukung']?></td>
                  <td><?php echo $key['dana_kampanye']?></td>
                   <td>
                  <a href="<?php echo base_url('paslon/lihat/'.$key['_id'])?>" class="btn btn-primary green"><i class="fa fa-eye"></i></a> 
                 
                  <a href="<?php echo base_url('paslon/edit/'.$key['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                   <a href="<?php echo base_url('paslon/hapus/'.$key['_id'])?>" class="btn btn-primary red"><i class="fa fa-trash"></i></a> 
                </td>
                </tr>
                <?php
              }
              ?>
            </tbody>

          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer'); ?>
<script>
  $(function () {
    $('#datatable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>