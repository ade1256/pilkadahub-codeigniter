<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Detail Berita</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo base_url('berita')?>"> Berita</a></li>
      <li class="active"><?php echo $berita['title'] ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
       <div class="box">
        <div class="box-body">
           <h1><?php echo $berita['title']; ?></h1>
           <br>
           <br>
           <?php echo htmlspecialchars_decode($berita['body']); ?>
           <a href="<?php echo base_url('berita/hapus/'.$berita['_id'])?>" class="btn btn-primary"><i class="fa fa-trash"></i> Hapus</a> 
           <a href="<?php echo base_url('berita/edit/'.$berita['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
   </section>
   <!-- /.content -->
 </div>
 <?php $this->load->view('layout/footer'); ?>
