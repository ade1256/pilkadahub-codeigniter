<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Tambah Agenda
   </h1>
   <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?php echo base_url('agenda') ?>"> Agenda</a></li>
    <li class="active"> Tambah Agenda</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Tambah Agenda</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form method="post" action="<?php echo base_url('agenda/kirim');?>">
            <div class="form-group">
              <label>Tanggal Awal</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="hidden" class="form-control" name="__v" value="0" />
                <input type="text" class="form-control pull-right" id="datepicker" name="tgl_awal" />
              </div>
            </div>
            <div class="form-group">
              <label>Tanggal Akhir</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right" id="datepicker2" name="tgl_akhir" />
              </div>
            </div>
            <div class="form-group">
              <label>Detail</label>
              <input class="form-control" placeholder="" name="detail"/>
            </div>
             <div class="form-group">
              <label>Tahapan</label>
             <select name="tahapan" class="form-control">
               <option value="Persiapan">Persiapan</option>
               <option value="Penyelenggaraan">Penyelenggaraan</option>
             </select>
            </div>
            <div class="form-group">
             <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer'); ?>
<script>
  $(function () {


    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd MM yyyy'
    });
    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy'
    });


  });
</script>
<script> $(function() { $('textarea').froalaEditor() }); </script>