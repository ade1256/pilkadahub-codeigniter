<?php $this->load->view('layout/header'); ?>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
<style type="text/css">
.btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  outline: none;
  background: white;
  cursor: inherit;
  display: block;
}

#img-upload{
 max-width: 300px;
}

.lds-ring {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
  top: 50%;
}
.lds-ring div {
  box-sizing: border-box;
  display: block;
  position: absolute;
  width: 51px;
  height: 51px;
  margin: 6px;
  border: 6px solid #fff;
  border-radius: 50%;
  animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  border-color: #fff transparent transparent transparent;
}
.lds-ring div:nth-child(1) {
  animation-delay: -0.45s;
}
.lds-ring div:nth-child(2) {
  animation-delay: -0.3s;
}
.lds-ring div:nth-child(3) {
  animation-delay: -0.15s;
}
@keyframes lds-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

}
</style>
<div id="loading" style="
display: none;
width:  100%;
height: 100%;
background: #0000009c;
color:  #fff;
position:  absolute;
z-index: 9999;
text-align: center;
">
<div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title ?></h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo base_url('aduan')?>"> Aduan</a></li>
      <li class="active"><?php echo $aduan['email'] ?></li>
    </ol>
  </section>

  <script type="text/javascript">
    var img = "";
    var upFoto = false;
    var config = {
      apiKey: "AIzaSyDdBwHnppn8R_JFmff8-vuMHxSf7OLgB6I",
      authDomain: "pilkadahub.firebaseapp.com",
      databaseURL: "https://pilkadahub.firebaseio.com",
      projectId: "pilkadahub",
      storageBucket: "pilkadahub.appspot.com",
      messagingSenderId: "56408364674"
    };
    firebase.initializeApp(config);

    function submitHandler(e){
      if (img != "") {
        if (upFoto == false) {
          e.preventDefault();
          $('#loading').css("display","block");
          $('body').css("overflow","hidden");
          var storageRef = firebase.storage().ref();
          var foto = ["<?=$aduan['photo'][1]?>","<?=$aduan['photo'][0]?>"];
          if( foto[0] == "") {
            foto[0] = firebase.database().ref().push().key;
            $("#idPhoto").val(foto[0]);
          }
          foto[1] = $("#photo").val();
          storageRef.child("laporan/"+foto[0]+"/"+foto[1])
          .putString(img, 'data_url').then(function(snapshot) {
            console.log("uploaded");
            upFoto = true;
            $("#form").submit();
          });
        }
      }
    }
  </script>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
       <div class="box">
        <div class="box-body">
          <form 
          id="form" 
          method="post" 
          action="<?php echo base_url('aduan/update') ?>" 
          onsubmit="submitHandler(event)">
          <div class="form-group">
            <label>Nama</label>
            <input type="hidden" name="id" class="form-control" value="<?php echo $aduan['_id'] ?>" />
            <input type="hidden" name="idPhoto" class="form-control" id="idPhoto" value="<?php echo $aduan['photo'][1] ?>" />
            <input type="text" name="nama" class="form-control" value="<?php echo $aduan['nama'] ?>" />
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <input type="text" name="alamat" class="form-control" value="<?php echo $aduan['alamat'] ?>" />
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control" value="<?php echo $aduan['email'] ?>" />
          </div>
          <div class="form-group">
            <label>No HP</label>
            <input type="number" name="no_hp" class="form-control" value="<?php echo $aduan['no_hp'] ?>" />
          </div>
          <div class="form-group">
            <label>Tipe</label>
            <select class="form-control" name="tipe">
             <option <?php if($aduan['tipe']=='Kritik' || $aduan['tipe']=='kritik'){echo 'selected';} ?> value="Kritik">Kritik</option>
             <option <?php if($aduan['tipe']=='Saran' || $aduan['tipe']=='saran'){echo 'selected';} ?> value="Saran">Saran</option>
           </select>
         </div>
         <div class="form-group">
          <label>Isi</label>
          <textarea type="text" name="isi" class="form-control"><?php echo $aduan['isi'] ?></textarea>
        </div>
        <div class="form-group">
          <label>Photo</label>
          <div class="input-group">
            <span class="input-group-btn">
              <span class="btn btn-default btn-file">
                Browse… <input type="file" id="imgInp" name="photo">
              </span>
            </span>
            <input type="text" class="form-control" name="photo" id="photo" value="<?=$aduan['photo'][0]?>" readonly>
          </div>  
          <?php echo '<img id="img-upload" src="https://firebasestorage.googleapis.com/v0/b/pilkadahub.appspot.com/o/laporan%2F'.$aduan['photo'][1].'%2F'.$aduan['photo'][0].'?alt=media"/>' ?>
        </div>

        <br>
        <br>
        <input type="submit" class="btn btn-primary" value="Simpan" />
        <a href="<?php echo base_url('aduan')?>" class="btn btn-primary">Batal</a>
      </form>
    </div>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer'); ?>
<script>
  $(document).ready( function() {
    $(document).on('change', '.btn-file :file', function() {
      var input = $(this),
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function(event, label) {

      var input = $(this).parents('.input-group').find(':text'),
      log = label;

      if( input.length ) {
        input.val(log);
      } else {
        if( log ) alert(log);
      }
      
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#img-upload').attr('src', e.target.result);
          img = e.target.result;
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imgInp").change(function(){
      readURL(this);
    });   
  });
</script>
