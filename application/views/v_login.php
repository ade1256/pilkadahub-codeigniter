<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $title; ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
	<!-- iCheck for checkboxes and radio inputs -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
	<!-- Bootstrap Color Picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
	<!-- Bootstrap time Picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
	<!-- Include Editor style. -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/style.css">
  	<script src="https://cdn.firebase.com/libs/firebaseui/2.5.1/firebaseui.js"></script>
  	<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
  	<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/2.5.1/firebaseui.css" />
  	<style type="text/css">
  	.panel-heading {
  		padding: 5px 15px;
  	}

  	.panel-footer {
  		padding: 1px 15px;
  		color: #A0A0A0;
  	}

  	.profile-img {
  		width: 96px;
  		height: 96px;
  		margin: 0 auto 10px;
  		display: block;
  		-moz-border-radius: 50%;
  		-webkit-border-radius: 50%;
  		border-radius: 50%;
  	}
  </style>
</head>
<body style="background-color: #f1f2f6">

	<div class="container" style="margin-top:40px;" >
		<div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<div class="logo">
					<img src="<?php echo base_url('assets/dist/img/logo_pilkada.png') ?>" />
				</div>
				<div class="caption">
					Silahkan login untuk mengakses dashboard PilkadaHub
				</div>
				<form role="form" method="POST" onsubmit="submithandler(event)">
					<div class="form-login">
						<label>Username</label>
						<input class="form-control form-login-style" id="email" placeholder="Email" name="email" type="email" autofocus />
						<label>Password</label>
						<input class="form-control form-login-style" placeholder="Password" name="password" type="password" id="password" />
					</div>
					<input type="submit" class="btn btn-lg btn-primary btn-block button-login" value="LOGIN">
				</form>
			</div>
		</div>
	</div>
	<div class="footer_login" style="background-image: url(<?php echo base_url('assets/dist/img/footer_login.svg'); ?>"></div>
<!-- <form role="form" method="POST" onsubmit="submithandler(event)">
							<fieldset>
								<div class="row">
									<div class="center-block">
										<img class="profile-img"
										src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-10  col-md-offset-1 ">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span> 
												<input class="form-control" id="email" placeholder="Email" name="email" type="email" autofocus>
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-lock"></i>
												</span>
												<input class="form-control" placeholder="Password" name="password" type="password" id="password">
											</div>
										</div>
										<div class="form-group">
											<input type="submit" class="btn btn-lg btn-primary btn-block" value="Sign in">
										</div>
									</div>
								</div>
							</fieldset>
						</form> -->
						<script>
							function submithandler(event){

								event.preventDefault();
								var config = {
									apiKey: "AIzaSyDdBwHnppn8R_JFmff8-vuMHxSf7OLgB6I",
									authDomain: "pilkadahub.firebaseapp.com",
									databaseURL: "https://pilkadahub.firebaseio.com",
									projectId: "pilkadahub",
									storageBucket: "pilkadahub.appspot.com",
									messagingSenderId: "56408364674"
								};
								firebase.initializeApp(config);

								var email = document.getElementById("email").value;
								var password = document.getElementById("password").value;

								document.cookie = "email="+email+";";
								document.cookie = "password="+password+";";

								<?php 
								$email =  $this->input->cookie('email');
								$password =  $this->input->cookie('password');
								$data_session = array(
									'status' => "login",
									'email' => $email,
									'level' => "admin",
									'auth' => "pilkadahub"
								);

								$this->session->set_userdata($data_session);
								?>

								firebase.auth().signInWithEmailAndPassword(email, password).then(function(success){
									window.location.href = "<?=base_url("home")?>";
								}).catch(function(error) {
									var errorCode = error.code;
									var errorMessage = error.message;
									if (errorCode === 'auth/wrong-password') {
										alert('Email/Password Salah');
									} else {   
										alert(errorMessage);

									}
									console.log(error);
									document.getElementById('quickstart-sign-in').disabled = false;
								});
							}
						</script>
						<!-- jQuery 2.2.3 -->
						<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
						<!-- Bootstrap 3.3.6 -->
						<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
						<!-- DataTables -->
						<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
						<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
						<!-- Select2 -->
						<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
						<!-- InputMask -->
						<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
						<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
						<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
						<!-- date-range-picker -->
						<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
						<script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
						<!-- bootstrap datepicker -->
						<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
						<!-- bootstrap color picker -->
						<script src="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
						<!-- bootstrap time picker -->
						<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
						<!-- SlimScroll 1.3.0 -->
						<script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
						<!-- iCheck 1.0.1 -->
						<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
						<!-- FastClick -->
						<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
						<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
						<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>

						<!-- Include Editor JS files. -->
						<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.0/js/froala_editor.pkgd.min.js"></script>
						<!-- AdminLTE App -->
						<script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
						<!-- AdminLTE for demo purposes -->
						<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
						<!-- page script -->

					</body>
					</html>
