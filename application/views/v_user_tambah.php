<?php $this->load->view('layout/header'); ?>
<script src="https://cdn.firebase.com/libs/firebaseui/2.5.1/firebaseui.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/2.5.1/firebaseui.css" />

<!-- isi -->

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $title ?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><?php echo $title ?></h3>
						<br/>
					</div>
					<script type="text/javascript">
						var config = {
							apiKey: "AIzaSyDdBwHnppn8R_JFmff8-vuMHxSf7OLgB6I",
							authDomain: "pilkadahub.firebaseapp.com",
							databaseURL: "https://pilkadahub.firebaseio.com",
							projectId: "pilkadahub",
							storageBucket: "pilkadahub.appspot.com",
							messagingSenderId: "56408364674"
						};
						firebase.initializeApp(config);

						firebase.auth().signInWithEmailAndPassword('ade1256@mail.com', 'ade12561256').catch(function(error) {
							var errorCode = error.code;
							var errorMessage = error.message;
							if (errorCode === 'auth/wrong-password') {
								alert('Wrong password.');
							} else {
								alert(errorMessage);
							}
							console.log(error);
							document.getElementById('quickstart-sign-in').disabled = false;
						});

						function submitHandler(e){
							e.preventDefault();
							var email = $("#email").val();
							var id_daerah = $("#id_daerah").val();
							var nama = $("#nama").val();
							var username = $("#username").val();
							var password = $("#password").val();

							var userData = {
								email: $("#email").val(),
								id_daerah: $("#daerah").val(),
								nama: $("#nama").val(),
								username: $("#username").val(),
							};
							firebase.auth().createUserWithEmailAndPassword(email, password)
							.then(function success(userdata){
								var uid = userdata.uid;
								firebase.database().ref().child("admin/"+uid).set(userData).then(function(){
									window.location.href = "<?=base_url("user")?>";
								});
							})
							.catch(function(error) {
								var errorCode = error.code;
								var errorMessage = error.message;
								alert(errorMessage);
								console.log(errorMessage);
							});
						}
					</script>
					<div class="box-body">
						<form  method="post" onsubmit='submitHandler(event)'>
							<table class="table table-hover margin-bottom-0">
								<tbody>
									<tr>
										<th>
											Nama
										</th>
										<td>
											<input type="text" name="nama" id="nama" class="form-control form-control-sm" required>
										</td>
									</tr>
									<tr>
										<th>
											Username
										</th>
										<td>
											<input type="text" name="username" id="username" class="form-control form-control-sm" required>
										</td>
									</tr>
									<tr>
										<th>
											Email
										</th>
										<td>
											<input type="text" name="email" id="email" class="form-control form-control-sm" required>
										</td>
									</tr>
									<tr>
										<th>
											Daerah
										</th>
										<td>
											<input type="text" name="Daerah" id="daerah" class="form-control form-control-sm" required>
										</td>
									</tr>
									<tr>
										<th>
											Password
										</th>
										<td>
											<input type="password" name="Password" id="password" class="form-control form-control-sm" required>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<input type='submit' class='btn btn-primary' value='Kirim'>	
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('layout/footer'); ?>