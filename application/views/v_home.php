<?php $this->load->view('layout/header');?>

<!-- Morris charts -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      PilkadaHub
      <small>Selamat datang di dashboard !</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active"><a href="<?php echo base_url('home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
       <!-- DONUT CHART -->
       <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Total Keseluruhan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body chart-responsive">
          <div class="chart" id="sales-chart" style="height: 150px; position: relative;"></div>
          <div class="kiri" style="float: left;width: 200px;">
            <p><span class="circle green"></span>Paslon 1</p>
            <h2><?php echo $suara['pwt1']['paslon']['id1'] ?></h2>
            <h6>H. GANJAR PRANOWO, S.H., M.IP - H. TAJ YASIN</h6>
          </div>
          <div class="kanan" style="float: right;width: 200px;">
            <p><span class="circle red"></span>Paslon 2</p>
            <h2><?php echo $suara['pwt1']['paslon']['id2'] ?></h2>
            <h6>SUDIRMMAN SAID - Dra. IDA FAUZIAH</h6>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <div class="col-md-6">
     <!-- DONUT CHART -->
     <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Pilih Menu</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body chart-responsive">
        <a href="<?php echo base_url('berita') ?>" class="btn btn-primary">Data Berita</a>
        <a href="<?php echo base_url('agenda') ?>" class="btn btn-primary">Data Agenda</a>
        <a href="<?php echo base_url('aduan') ?>" class="btn btn-primary">Data Aduan</a>
        <a href="<?php echo base_url('user') ?>" class="btn btn-primary">Data User</a>
        <a href="<?php echo base_url('paslon') ?>" class="btn btn-primary">Data Paslon</a>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <!-- BAR CHART -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Berdasarkan Daerah</h3>
        <p>Silahkan klik untuk melihat lebih detail</p>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body chart-responsive">
        <div class="chart" id="bar-chart" style="height: 300px;"></div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>

</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer');?>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.min.js"></script>

<script>
  $(function () {
    "use strict";
      //DONUT CHART
      var donut = new Morris.Donut({
        element: 'sales-chart',
        resize: true,
        colors: ['#f56954','#00a65a'],
        data: [
        {label: "PASLON 1", value: <?php echo $suara['pwt1']['paslon']['id1'] ?>},
        {label: "PASLON 2", value: <?php echo $suara['pwt1']['paslon']['id2'] ?>},
        ],
        hideHover: 'auto'
      });
    //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart',
      resize: false,
      data: [
      {y: 'Banjarnegara', a: 75, b: 65},
      {y: 'Banyumas', a: <?php echo $suara['pwt1']['paslon']['id1'] ?>, b: <?php echo $suara['pwt1']['paslon']['id2'] ?>},
      {y: 'Batang', a: 75, b: 65},
      {y: 'Blora', a: 50, b: 40},
      {y: 'Boyolali', a: 75, b: 65},
      {y: 'Brebes', a: 50, b: 40},
      {y: 'Cilacap', a: 75, b: 65},
      {y: 'Demak', a: 100, b: 90},
      {y: 'Grobogan', a: 1200, b: 1190},
      {y: 'Jepara', a: 100, b: 90},
      {y: 'Karanganyar', a: 100, b: 90},
      {y: 'Kebumen', a: 100, b: 90},
      {y: 'Kendal', a: 100, b: 90},
      {y: 'Klaten', a: 100, b: 90},
      {y: 'Kudus', a: 100, b: 90},
      {y: 'Magelang', a: 100, b: 90},
      {y: 'Pati', a: 100, b: 90},
      {y: 'Pekalongan', a: 100, b: 90},
      {y: 'Pemalang', a: 100, b: 90},
      {y: 'Purbalingga', a: 100, b: 90},
      {y: 'Purworejo', a: 100, b: 90},
      {y: 'Rembang', a: 100, b: 90},
      {y: 'Salatiga', a: 100, b: 90},
      {y: 'Semarang', a: 100, b: 90},
      {y: 'Sragen', a: 100, b: 90},
      {y: 'Sukoharjo', a: 100, b: 90},
      {y: 'Surakarta', a: 100, b: 90},
      {y: 'Tegal', a: 100, b: 90},
      {y: 'Temanggung', a: 100, b: 90},
      {y: 'Wonogiri', a: 100, b: 90},
      {y: 'Wonosobo', a: 100, b: 90},
      ],
      barColors: ['#f56954','#00a65a'],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['PASLON 1', 'PASLON 2'],
      // axes:false,
      hideHover: 'auto'
    }).on('click', function(i, row) {        
     window.location = "<?php echo base_url('home/quickcount/') ?>"+row.y;     
   });
  });
</script>