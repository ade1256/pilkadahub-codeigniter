<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title ?></h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo base_url('paslon')?>"> Paslon</a></li>
      <li class="active">Tambah Paslon</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
       <div class="box">
        <div class="box-body">
         <form method="post" action="<?php echo base_url('paslon/kirim') ?>" >
          <h2>Form Kepala Daerah</h2>
          <div class="form-group">
            <label>Nama Kepala Daerah</label>
            <input type="text" name="nama_kepala_daerah" class="form-control" />
          </div>
          <div class="form-group">
            <label>Jenis Kelamin</label>
            <select class="form-control" name="gender_kepala_daerah">
             <option value="LAKI-LAKI">LAKI-LAKI</option>
             <option value="PEREMPUAN">PEREMPUAN</option>
           </select>
         </div>
         <div class="form-group">
          <label>Tempat Lahir</label>
          <input type="text" name="tempat_lahir_kepala_daerah" class="form-control" />
        </div>
        <div class="form-group">
          <label>Tanggal Lahir</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right" id="datepicker" name="tanggal_lahir_kepala_daerah" />
          </div>
        </div>
        <div class="form-group">
          <label>Pekerjaan</label>
          <input type="text" name="pekerjaan_kepala_daerah" class="form-control" />
        </div>
        <h2>Form Wakil Kepala Daerah</h2>
        <div class="form-group">
          <label>Nama Wakil Kepala Daerah</label>
          <input type="text" name="nama_wakil_kepala_daerah" class="form-control" />
        </div>
        <div class="form-group">
          <label>Jenis Kelamin</label>
          <select class="form-control" name="gender_wakil_kepala_daerah">
           <option value="LAKI-LAKI">LAKI-LAKI</option>
           <option value="PEREMPUAN">PEREMPUAN</option>
         </select>
       </div>
       <div class="form-group">
        <label>Tempat Lahir</label>
        <input type="text" name="tempat_lahir_wakil" class="form-control" />
      </div>
      <div class="form-group">
          <label>Tanggal Lahir</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right" id="datepicker2" name="tanggal_lahir_wakil" />
          </div>
        </div>
        <div class="form-group">
          <label>Pekerjaan</label>
          <input type="text" name="pekerjaan_wakil_kepala_daerah" class="form-control" />
        </div>
      
      <h1>Detail PASLON</h1>
      <div class="form-group">
        <label>Jumlah Dukungan</label>
        <input type="text" name="jumlah_dukungan" class="form-control" />
      </div>
      <div class="form-group">
        <label>Partai Pendukung</label>
        <input type="text" name="partai_pendukung" class="form-control" />
      </div>
      <div class="form-group">
        <label>Dana Kampanye</label>
        <input type="text" name="dana_kampanye" class="form-control" />
      </div>
      <input type="submit" class="btn btn-primary" value="Simpan" />
      <a href="<?php echo base_url('paslon')?>" class="btn btn-primary">Batal</a>
    </form>
  </div>
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer'); ?>
<script>
  $(function () {


    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy'
    });
    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy'
    });


  });
</script>

