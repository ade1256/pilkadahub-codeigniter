<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title ?></h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo base_url('aduan')?>"> Aduan</a></li>
      <li class="active"><?php echo $aduan['email'] ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
       <div class="box">
        <div class="box-body">
           <p>Nama <?php echo $aduan['nama'] ?></p>
           <p>Alamat <?php echo $aduan['alamat'] ?></p>
           <p>Email <?php echo $aduan['email'] ?></p>
           <p>No Hp <?php echo $aduan['no_hp'] ?></p>
           <p>Tipe <?php echo $aduan['tipe'] ?></p>
           <br>
           <br>
           <p><?php echo $aduan['isi'] ?></p>
           <?php echo '<img width="300" src="https://firebasestorage.googleapis.com/v0/b/pilkadahub.appspot.com/o/laporan%2F'.$aduan['photo'][1].'%2F'.$aduan['photo'][0].'?alt=media"/>' ?>
           <br>
           <br>
           <a href="<?php echo base_url('aduan/hapus/'.$aduan['_id'])?>" class="btn btn-primary"><i class="fa fa-trash"></i> Hapus</a> 
           <a href="<?php echo base_url('aduan/edit/'.$aduan['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
   </section>
   <!-- /.content -->
 </div>
 <?php $this->load->view('layout/footer'); ?>
