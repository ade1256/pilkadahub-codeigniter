<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title ?></h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo base_url('agenda')?>"> Agenda</a></li>
      <li class="active"><?php echo $agenda['detail'] ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
       <div class="box">
        <div class="box-body">
           <h1><?php echo $agenda['detail']; ?></h1>
           <br>
           <p>Tanggal Awal : <?php echo $agenda['tgl_awal'] ?></p>
           <p>Tanggal Akhir : <?php echo $agenda['tgl_akhir'] ?></p>
           <br>
           <br>
           <a href="<?php echo base_url('agenda/hapus/'.$agenda['_id'])?>" class="btn btn-primary"><i class="fa fa-trash"></i> Hapus</a> 
           <a href="<?php echo base_url('agenda/edit/'.$agenda['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
   </section>
   <!-- /.content -->
 </div>
 <?php $this->load->view('layout/footer'); ?>
