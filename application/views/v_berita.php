<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Berita
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data Berita</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Berita</h3>
            <br/>
            <br/>
            <a href="<?php echo base_url('berita/tambah') ?>" class="btn btn-primary">Tambah Data</a>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="datatable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Created at</th>
                  <th>Title</th>
                  <th>Body</th>
                  <th>Tag</th>
                  <th>Image Link</th>
                  <th>Visitor</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                foreach ($berita as $news) {
                  ?>
                  <tr>
                    <td><?php echo substr($news['_id'], 0,10).'...' ?></td>
                    <td><?php echo $news['created_at']?></td>
                    <td><?php echo substr($news['title'], 0,20).'...' ?></td>
                    <td><?php echo substr(strip_tags(html_entity_decode($news['body'])), 0, 30).'...' ?></td>
                    <td><?php echo $news['tag']?></td>
                    <td><?php echo substr($news['img_link'], 0,20).'...' ?></td>
                    <td><?php echo $news['visitor']?></td>
                    <td>
                      <a href="<?php echo base_url('berita/lihat/'.$news['_id'])?>" class="btn btn-primary green"><i class="fa fa-eye"></i></a> 
                      <a href="<?php echo base_url('berita/edit/'.$news['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                       <a href="<?php echo base_url('berita/hapus/'.$news['_id'])?>" class="btn btn-primary red"><i class="fa fa-trash"></i></a> 
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <?php $this->load->view('layout/footer'); ?>
  <script>
    $(function () {
      $('#datatable').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });
    });
  </script>