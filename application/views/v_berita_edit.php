<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Edit Berita
   </h1>
   <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?php echo base_url('berita') ?>"> Berita</a></li>
    <li class="active"> Edit Berita</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Edit Berita</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form method="post" action="<?php echo base_url('berita/update');?>">
            <div class="form-group">
              <label>Created at</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="hidden" class="form-control" name="_id" value="<?php echo $berita['_id'] ?>" />
                <input type="text" class="form-control pull-right" id="datepicker" name="tanggal" value="<?php echo $berita['created_at'] ?>" />
              </div>
            </div>
            <div class="form-group">
              <label>Title</label>
              <input class="form-control" placeholder="" name="title" value="<?php echo $berita['title'] ?>" />
            </div>
            <div class="form-group">
              <label>Body</label>
              <textarea name="body"><?php echo $berita['body'] ?></textarea>
            </div>
            <div class="form-group">
              <label>Tag</label>
              <input class="form-control" placeholder="" name="tag" value="<?php echo $berita['tag'] ?>"/>
            </div>
            <div class="form-group">
              <label>Image Link</label>
              <input class="form-control" placeholder="" name="img_link" value="<?php echo $berita['img_link'] ?>" />
            </div>
            <div class="form-group">
              <label>Visitor</label>
              <input class="form-control" placeholder="" name="visitor" value="<?php echo $berita['visitor'] ?>"/>
            </div>
            <div class="form-group">
             <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer'); ?>
<script>
  $(function () {


    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy'
    });


  });
</script>
<script> $(function() { $('textarea').froalaEditor() }); </script>