<?php $this->load->view('layout/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo $title ?> <?php echo $paslon['nama_kepala_daerah'].' - '.$paslon['nama_wakil_kepala_daerah'] ?></h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dahsboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url('paslon')?>"> Paslon</a></li>
			<li class="active"><?php echo $paslon['nama_kepala_daerah'].' dan '.$paslon['nama_wakil_kepala_daerah'] ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body">
						<table>
							<tr><td>KEPALA - WAKIL</td><td>:</td><td><?php echo $paslon['nama_kepala_daerah'].' - '.$paslon['nama_wakil_kepala_daerah'] ?></td></tr>
							<tr><td>GENDER</td><td>:</td><td><?php echo $paslon['gender_kepala_daerah'].' - '.$paslon['gender_wakil_kepala_daerah'] ?></td></tr>
							<tr><td>TEMPAT LAHIR</td><td>:</td> <td><?php echo $paslon['tempat_lahir_kepala_daerah'].' - '.$paslon['tempat_lahir_wakil'] ?></td></tr>
							<tr><td>TANGGAL LAHIR</td><td>:</td><td><?php echo $paslon['tanggal_lahir_kepala_daerah'].' - '.$paslon['tanggal_lahir_wakil'] ?></td></tr>
							<tr><td>PEKERJAAN</td><td>:</td><td><?php echo $paslon['pekerjaan_kepala_daerah'].' - '.$paslon['pekerjaan_wakil_kepala_daerah'] ?></td></tr>
							<tr><td>JUMLAH DUKUNGAN</td><td>:</td><td><?php echo $paslon['jumlah_dukungan']?></td></tr>
							<tr><td>PARTAI PENDUKUNG</td><td>:</td><td><?php echo $paslon['partai_pendukung']?></td></tr>
							<tr><td>DANA KAMPANYE</td><td>:</td><td><?php echo $paslon['dana_kampanye']?></td></tr>
							<tr><td>ACTION</td><td>:</td><td><a href="<?php echo base_url('paslon/hapus/'.$paslon['_id'])?>" class="btn btn-primary"><i class="fa fa-trash"></i></a> 
								<a href="<?php echo base_url('paslon/edit/'.$paslon['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td></tr>
							</table>
						</div>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<?php $this->load->view('layout/footer'); ?>
